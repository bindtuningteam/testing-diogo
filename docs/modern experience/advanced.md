![Add section](../images/modern/03.advancedoptions.png)

___
### Always show BT Buttons

When this is **turned on** the icons to manage the content of the Web Part are always visible when the page is in edit mode.<br>
When the feature is **turned off** you need to hover the web part to see the controls

___
### Title Link 

Note this option is only valid if you have a Title on the Web Part.<br>
On the **Title** of the Web Part, you can add an URL to redirect you to any page. The Web Part will always open the new URL in a new tab of your browser and that option is only visible if you're not in **Edit** mode.

___
### Allow users to upload images
 
 If you activate this option, users with permissions to manage the connected document library, can upload images by dragging and dropping it to the WebPart zone.

 <p class="alert alert-warning">
<strong>Warning:</strong> There is a 2MB size limit per file. Uploads only are available if the options Target Folder on Folder Options and/or Folder Structure on Layout Options are activated.
 </p>
 
___
### Show upload button
 
 If you activate this option, an upload button will be available for the users that can upload images.