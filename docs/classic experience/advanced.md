![insert-tab](../images/classic/32.advancedoptions.png)

___
### Allow users to upload images
 
 If you activate this option, users with permissions to manage the connected document library, can upload images by dragging and dropping it to the WebPart zone.

 <p class="alert alert-warning">
<strong>Warning:</strong> There is a 2MB size limit per file. Uploads only are available if the options Target Folder on Folder Options and/or Folder Structure on Layout Options are activated.
 </p>
 
___
### Show upload button
 
 If you activate this option, an upload button will be available for the users that can upload images.

___
### Language

From here you can select which language you want to be displayed by the web part. This will also translate the forms, but a refresh may be required before you see the changes.

By default, the BindTuning Web Part provides the language for:

- English
- Portuguese

If you'd like to localize the web part to your own language, please refer to [this article](https://support.bindtuning.com/hc/en-us/articles/115004585263).